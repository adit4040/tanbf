#Problem 1
age=23
age_in_days=format(age*365,",d")
age_in_weeks=format(age*52,",d")
message="Ben is {} years old, which is {} days old, or {} weeks old."

print(message.format(age,age_in_days,age_in_weeks))

#Problem 2
print("*"*48)
distance_nyc=216
bike_speed=15
time= distance_nyc/bike_speed
message2="New York City is {} miles away. With my average bike speed of {} mph, it should take me {} hours, or {} day(s) to get from Boston to NYC"

print(message2.format(distance_nyc,bike_speed,time, time/24))

#Problem 3
print("*"*48)
attendance_lastyear=12000
attendance_thisyear=18000
cost_lastyear=250
cost_thisyear=299
revenue_lastyear=cost_lastyear*attendance_lastyear
revenue_thisyear=cost_thisyear*attendance_thisyear
message3="Boston Calling is expecting {} more people this year. They earned ${} last year and they expect to earn ${} this year. This is an increase of ${}."

print(message3.format(format(attendance_thisyear-attendance_lastyear,",d"),
        "{:,d}".format(revenue_lastyear),
        format(revenue_thisyear,",d"),
        format(revenue_thisyear-revenue_lastyear,",d")))
