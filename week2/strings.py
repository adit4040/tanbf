#Thugz Mansion

#Part1
title="Thugz Mansion"
artist="2Pac"
line1="Ain't no place I'd rather be"
line2="Chillin' with homies and family"
line3="Sky high, iced out paradise"
line4="In the sky"
line5="Ain't no place I'd rather be"
line6="Only place that's right for me"
line7="Chromed-out mansion in paradise"
line8="In the sky"

print(title)
print("by"+" "+artist)
print(" ")
print(line1)
print(line2)
print(line3)
print(line4)
print(line5)
print(line6)
print(line7)
print(line8)

#Part2
print("*"*48)

info=title+" "+"by" +" "+artist
content= line1+" "+line2+" "+line3+" "+line4+" "+line5+" "+line6+" "+line7+" "+line8
print(info)
print(" ")
print(content)
#Part3
print("*"*48)
greeting="My favorite song is {} by {} and my favorite quote is {} characters in length."
length=len(content)

print(greeting.format(title,artist,length))
