season=open("seasons.txt", "w") #clear the txt from previous sessions
season.close()
season=open("seasons.txt", "a")

acceptable_answers=["fall","Fall","winter","Winter","spring","Spring","summer","Summer","quit"]
answer=''
while answer !="quit":
	answer=input("What is your favorite season of the year? ")
	if answer not in acceptable_answers:
		print("Please give a proper input.") #Does not write answer to txt file
	elif answer=="quit":
		break #Prevent 'quit' from being written
	else:
		season.write(answer +"\n")

season.close()
season=open("seasons.txt","r")
lines=season.readlines()

#store results in a list
answerlist=[]
for line in lines:
	answerlist.append(line)

fall=0
winter=0
spring=0
summer=0
#Most efficient solution
'''
for line in lines:
	if line=="fall\n":
		fall+=1
	elif line=="winter\n":
		winter+=1
	elif line=="spring\n":
		spring+=1
	elif line=="summer\n":
		summer+=1
'''
#Another solution would be to pop the list items into variables but doesnt seem necessary
#solution putting lines into list and then reading from the list
for item in answerlist:
	if item=="fall\n" or item=="Fall\n":
		fall+=1
	elif item=="winter\n" or item=="Winter\n":
		winter+=1
	elif item=="spring\n" or item=="Spring\n":
		spring+=1
	elif item=="summer\n" or item=="Summer\n":
		summer+=1
	
print("fall: " + str(fall))
print("winter: "+ str(winter))
print("spring: "+ str(spring))
print("summer: "+ str(summer))

season.close()