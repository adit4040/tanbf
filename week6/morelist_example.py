list_example=[1,2,3,4,5,6,7,8,9]

del list_example[-1]
del list_example[0]
print(list_example)

list_example.insert(3,99)
print(list_example)

list_example.remove(8)
list_example.remove(7)
list_example.remove(6)
list_example.remove(5)
list_example.remove(99)

print(list_example)
var1=list_example.pop(0)
var2=list_example.pop()
var3=list_example.pop()
print(list_example)

print(var1+var2+var3)