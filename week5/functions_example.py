#Problem 7 :Functions

#Problem 1
def age_indays(age):
	return format(age*365,",d")
#Interactive
#user_age=int(input("How old are you? "))
user_age=23
print("Your age is " + age_indays(user_age) + " days")

#Problem 2
print("*"*48)
def monthly_bill(gas,electric,cable,internet,phone):
	total=float(gas)+float(electric)+float(cable)+float(internet)+float(phone)
	return "Your total monthly bill is " + str(total) + " dollars."
	
print(monthly_bill(1,2.2,3.3,4.4,5.5))