#Assignment 7:Loops

#Part1: Multiplication Table
num=9
for x in range(1,13):
	print(str(num)+" X " + str(x) + " =" + str(num*x))

#Part2:Multiplication table again
print("*"*48)
num=int(input("What number would you like the times table for? "))

for x in range(1,13):
	print(str(num)+" X " + str(x) + " =" + str(num*x))

#Part 3: Adding up until exit
print("*"*48)
user_input=''
total=0
print("You input will be added until you input 0")
while user_input!=0:
	user_input=int(input("Input number: "))
	total=total+ user_input
	print("The running total is: "+ str(total) )

#Part 4: Track workout
print("*"*48)
laps_ran=0
answer='n'
name="Ben"
laps=int(input("How many laps are you running today? "))

while answer =="n":
	answer=input("Are you done yet? [y/n]")
	laps_ran=laps_ran+1

laps_left=laps-laps_ran

if laps_ran==laps:
	print ("Nice job {} , You fly like an eagle!".format(name))
elif laps_ran>laps:
	laps_left=laps_left*-1
	print("You ran {} laps too many!".format(laps_left))
else:
	print("You are in trouble, go back to work. You have {} of laps left".format(laps_left))
