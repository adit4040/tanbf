#Problem 6: While Loops

#Problem 1 
age=1
while age <=21:
	if age ==16:
		milestone="You can drive now."
	elif age ==18:
		milestone="You can vote now."
	elif age ==21:
		milestone="You can drink now"
	else:
		milestone=""
	print("You are " + str(age) + " years old. " +milestone )
	age +=1

#Problem 2
print("*"*48)
password="" #password is empty
#do with two variables for parameters
while password!="Ji8ETaC28h4na7NoT":#correct password
	password= input("What is your password?: ")
	if password=="Ji8ETaC28h4na7NoT":
		print("That is your correct password.")
	else:
		print("That is the incorrect password. Please try again.")