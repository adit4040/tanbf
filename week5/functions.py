#Assignment 8: Functions

#Part1: Perimeter of a Square
def perimeter_sq(side1,side2,side3,side4):
	if side1!=side2 or side1!=side3 or side1!=side4 or side2!=side3 or side2!=side4 or side3!=side4:
		return "That is not a square"
	else:
		return "The perimeter of that square is: "+ str(side1+side2+side3+side4)

side1=int(input("What is the length of side1? "))
side2=int(input("What is the length of side2? "))
side3=int(input("What is the length of side3? "))
side4=int(input("What is the length of side4? "))

print(perimeter_sq(side1,side2,side3,side4))

#Part 2: Area of a triangle
print ("*"*48)
def area_tri(b,h):
	return (b*h)/2

b=int(input("What is the base of the triangle? "))
h=int(input("What is the height of the triangle? "))

print("The area of the triangle is: " +str(area_tri(b,h)))

#Part 3: Total Cost
print ("*"*48)
def total_cost(units,price):
	return int(units)*int(price)

units=input("How many units are being produced? ")
price=input("How much does each unit cost? ")

print("The total costs are " + str(total_cost(units,price)) + " dollars")