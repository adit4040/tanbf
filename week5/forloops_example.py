#Problem 5: For loops

#Problem 1
for num in list(range(1,31)):
	print(num**3)

#Problem 2
print("*"*48)
for num in range(1,21):
	if num%2==0:
		print(num)

#Problem 3
print("*"*48)
for num in range(1,101):
	if num ==24:
		continue
	if num == 81:
		break
	print(num)