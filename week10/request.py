
#import necessary modules
import requests
import csv

#get json from api
r=requests.get("https://jsonplaceholder.typicode.com/posts")
data=r.json()

#ask the user which line of the json they would like to write to csv
try:
    line=int(input("Which entry would you like to write to csv? "))

    #grab data based on user's request and store into variables
    userId=data[line]["userId"]
    identity=data[line]["id"]
    title=data[line]["title"]
    body=data[line]["body"]
    #identify fields to become headers
    fields=["userId","id","title","body"]
    try:
        #try to see if csv already exists. if it does not write a new one with headers
        open("data.csv", "r")
    except FileNotFoundError:
        with open("data.csv", "w") as datacsv:
            datawriter=csv.DictWriter(datacsv,fieldnames=fields)
            datawriter.writeheader()

    #append method to write multiple lines each time the program is run
    with open("data.csv", "a") as datacsv:
        datawriter=csv.DictWriter(datacsv,fieldnames=fields)
        datawriter.writerow({"userId":userId,"id":identity,"title":title,"body":body})

except ValueError:
    print("Please enter an integer.")
except IndexError:
    print("Please enter another number. That entry number is too high.")
