from flask import Flask
app=Flask(__name__)


@app.route('/square/<int:side1>/<int:side2>/<int:side3>/<int:side4>')
def perimeter_sq(side1,side2,side3,side4):
	if side1!=side2 or side1!=side3 or side1!=side4 or side2!=side3 or side2!=side4 or side3!=side4:
		return "<h2>That is not a square</h2>"
	else:
		return "<h2>The perimeter of a square sides of {} units is {} units.</h2>".format(side1,side1+side2+side3+side4)
		

@app.route('/triangle/<int:b>/<int:h>')
def area_tri(b,h):
	return "<h3> The area of the triangle with a base of {} units and a height of {} units is {} units squared.</h3>".format(b,h,(b*h)/2)

@app.route('/totalcost/<int:units>/<int:price>')
def total_cost(units,price):
	return "<h1> The total cost of {} units at a cost of ${} each is ${} </h1>".format(units,price,units*price)

app.run(debug=True, port=8000, host='0.0.0.0')

