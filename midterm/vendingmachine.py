print("Welcome to the Vendomatic. \n Type 'quit' at anytime to end your transacation \n Type 'catalog' to show avaliable items\n Type the name of an item to add it to your cart \n Type 'cart' to see your current cart \n Type 'checkout' to purchase items and see yout total")

catalog={"chips":1,"cheese":1,"filet mignon":17, "apple":1,"water":1,"grape soda":2,"beer":4,"orange juice":2}
cart=[]
total=0
command=''
while command !="quit":
	if not catalog: #check if no more items. empty dictionaries evaluate as false
		print("There are no more items. You will be checked out.")
		print("You are purchasing {} items for a total of ${}".format(len(cart),total))
		break

	command=input("What is your command? ")
	
	if command == "catalog":
		if not catalog: #if catalog is empty
			print("There are no more avaliable items")
		else:
			print("The avaliable drinks and snacks are:")
			for item in catalog: #print catalog
				print(item + "-$"+str(catalog[item]))
	elif command== "quit":
		pass
	elif command=="cart":
		if not cart: #if cart is empty
			print("Your cart is empty.")
		else:
			print("Your cart contains" + str(cart)) #print what is in cart and the cost
			print("The total cost in your cart is ${}".format(total))
	elif command =="checkout":
		if not cart: #cannot checkout if cart is empty
			print("Your cart is empty. Please select something before checking out")
		else:
			print("You are purchasing {} items for a total of ${}".format(len(cart),total)) #show total and clear cart
			cart=[]
			total=0
			print("You are free to make another transaction or type 'quit'")
	elif command in catalog :
		cart.append(command) #add purchased item to cart
		print("That item cost $ {}".format(catalog[command]))
		total+=catalog[command] #add cost of item to total
		print("The total cost in your cart is ${}".format(total))
		del catalog[command]#delete item from catalog
		print("Your cart contains" + str(cart))
	else:
		print("Invalid command. Please check spelling or item might not be avaliable.")

print("Thank you for using the Vendomatic. Have a great day!")
