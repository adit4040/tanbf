try: #try creating file to store list in.
	todolist=open("todo_list.txt","x")
except FileExistsError: #if lists already exists read what is in the list
	todolist=open("todo_list.txt", "r")
	lines=todolist.readlines()
	
	#check if list is empty. if it is, then do not print contents of list
	if len(lines)!=0:
		print("These are the items you have left to do. You can do it...Don't live for tomorrow what you can do today!")
		for line in lines:
			print(line) 


print("Welcome to ToDo.")

menutext="Choose one of the following options or just type what you want to do :\n Menu - show menu options \n Show - Show todo list \n Delete - delete item on the list\n Quit - quit adding to list"

print(menutext)

menu=["Menu","menu"]
show=["Show","show"]
delete=["Delete","delete"]
quit=["Quit","quit"]

command =''
while command not in quit:
	command = input("What do you want to remember to do? ")
	if command in menu:
		print(menutext)
	elif command in show:
		#reopen lines and print lines	
		#This has to go first in this sub because lines need to be read again. ie: from delete to show
		todolist=open("todo_list.txt","r")
		lines=todolist.readlines()
		#what to do if list is empty 
		if len(lines)==0:
			print("There are no items in your todo list.")
		for line in lines:
			print(line)
		todolist.close()
	elif command in delete:
		todolist=open("todo_list.txt","r")
		lines=todolist.readlines() #update current line count
		if len(lines)==0:
			print("There are no items in your list to delete")
		else:
			todolist.close()
			print("This is what is currently on your list: ")
			for line in lines: #show list so user can select what to delte
				print(line)
			#prompt user to select what to delete	
			todelete=str(input("What do you want to delete from your list? "))	
			todolist=open("todo_list.txt","w")
			if todelete in lines:
					print("test")
			#rewrite entire list to txt unless it matches the user's request to delete
			#This will delete multiples if they are exactly the same
			for line in lines:
				if line!=todelete + "\n":
					todolist.write(line)
			todolist.close()
	elif command in quit:
		break #prevent quit from being written into the text
	else:
		todolist=open("todo_list.txt", "a")
		todolist.write(command +"\n")
		todolist.close()
		todolist=open("todo_list.txt","r")
		lines=todolist.readlines()
		todolist.close()
		print("Update Successful. There are currently {} items in your list.".format(str(len(lines))))


print("Thank you for using ToDo. Your list will be saved")
todolist.close()