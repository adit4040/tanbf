import random

n=random.randint(1,100)


print("Welcome to the Numbertron. \n To win: Guess a random integer between 1 and 100 (inclusive) \n To quit: type 'quit'")

def direction_function(guess):
	if guess-n >0:
		return "lower"
	elif guess-n <0:
		return "higher"
def temp_function(guess):
	difference=guess-n
	absolutevalue=abs(difference)
	if absolutevalue>=25:
		return "cold"
	elif absolutevalue <=5:
		return "hot"
	elif absolutevalue<=15:
		return "warm"
	else:
		return "none"


try:
	guess=''
	while guess != "quit":
		guess=input("What is your guess? ") #prompt user for the number
		if guess=="quit" : #print friendly message upon quit
			print("Don't give up like a sore loser!")
		elif int(guess)==n: #check if guess is correct
			print("You are correct. You win $100.\nThank you for playing!")
			break
		elif int(guess) >100 or int(guess)<1: # check if guess is in range
			print("Please guess a number between 1 and 100")
		else:
			guess=int(guess)
			direction=direction_function(guess)
			temp=temp_function(guess)
			if temp == "none": #If the guess is between 25 and 15 off
				pass #no message is printed about hot/cold/warm
			else:
				print("You are {}".format(temp))#print hot/cold/warm message
			print("You need to guess {}".format(direction))#print up/down
except ValueError:
	print ("Please only enter integers. Game will end")

