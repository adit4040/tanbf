#Problem1

#Questions
question1="Describe the project you've worked on that you're most proud of. What did you do that worked out particularly well? "
question2="Describe the project you've worked on that you're least proud of. What would you do differently? "
question3="Give me an example of a time you had to take a creative and unusual approach to solve coding problem. " 
linebreak="\n"
#Ask questions and store in variables
answer1=input(question1+linebreak)
print(linebreak)
answer2=input(question2+linebreak)
print(linebreak)
answer3=input(question3+linebreak)

#Space out results
print(linebreak*2)
print("*"*20)
#print answers to Questions
print("Your responses: ")
print(linebreak)
print(question1+ linebreak +answer1)
print(linebreak)
print(question1+ linebreak +answer2)
print(linebreak)
print(question1+ linebreak +answer3)


#Problem2
#Ask question and store in age
question4="How many years old are you? "
age=int(input(question4))

print(linebreak)
#calculate age in weeks
age_weeks=age*52
#print response
print("You are " + str(age_weeks) +" weeks old")
