#problem3

#create list
traffic_light=["red","yellow","green"]
print(traffic_light)
#extend color meanings
traffic_light.extend(["stop","speed up","go"])
print(traffic_light)
#add pink
traffic_light.append("pink")
print(traffic_light)
#remove pink
traffic_light.remove("pink")
print(traffic_light)