#Part 1
#question list and answer list
question_list=["Are you chewing life's gristle?", "Does your life seem jolly rotten?", "Are you feeling in the dumps?", "Is your life quite absurd?"]
answer_list=["Don't grumble, give a whistle.","There's something you forgot?", "Don't be silly chumps.", "Death's the final word."]

#solution without loops
input(question_list[0]+ " ")
print(answer_list[0])
input(question_list[1]+ " ")
print(answer_list[1])
input(question_list[2]+ " ")
print(answer_list[2])
input(question_list[3]+ " ")
print(answer_list[3])

#alternative with  an FOR loop to be lazy/efficient
'''
for x in range(0,4): 
	input(question_list[x]+ " ")
	print(answer_list[x])
'''

#Part 2
print("*"*48)
response_true=["True","true","t"]
response_false=["False","false","f"]
falseanswer="I'm happy for you."
correct_input="Please answer with either True or False"

#I couldn't get it to work with inputs as booleans and not strings.
#Solution without loops
answer1=input(question_list[0]+ " ")
if answer1 in response_true:
	print(answer_list[0])
elif answer1 in response_false:
	print(falseanswer)
else:
	print(correct_input)

answer2=input(question_list[1]+ " ")
if answer2 in response_true:
	print(answer_list[1])
elif answer2 in response_false:
	print(falseanswer)
else:
	print(correct_input)

answer3=input(question_list[2]+ " ")
if answer3 in response_true:
	print(answer_list[2])
elif answer3 in response_false:
	print(falseanswer)
else:
	print(correct_input)

answer4=input(question_list[3]+ " ")
if answer4 in response_true:
	print(answer_list[3])
elif answer4 in response_false:
	print(falseanswer)
else:
	print(correct_input)

'''
#More elegant loop solution
#In this case the input is stored in answer0, answer1 etc
for x in range(0,4): 
	answerx=input(question_list[x]+ " ")
	if answerx in response_true:
		print(answer_list[x])
	elif answerx in response_false:
		print(falseanswer)
	else:
		print(correct_input)
	
'''