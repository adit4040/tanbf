#Try/catch to catch error when inputs a string for pats_score. Not sure if best way to accomplish this

try:
	pats_score=int(input("What is the Patriots score? "))
	falcons_score=28
	point_diff=falcons_score-pats_score

	print("The Falcons Score is {} ".format(falcons_score))

	if point_diff<0:
		print("Let's go Patriots!")
	elif point_diff>8:
		print("We are doomed. Goodbye Tom Brady")
	elif point_diff==7:
		print("The Patriots would need at least a touchdown with a two-point conversion. \nThey can also score a touchdown and one-point conversion to tie the game. \nIf they manage to get the ball back they can make a field goal or another touchdown for a miracle.")
	elif point_diff==0:
		print("The game is tied up.The Patriots would need at least a field goal to win.")
	elif point_diff==6:
		print("The Patriots would need at least a touchdown to win.")

except ValueError:
	print("Please only use integers for the score.")
